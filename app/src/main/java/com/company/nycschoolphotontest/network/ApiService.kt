package com.company.nycschoolphotontest.network

import com.company.nycschoolphotontest.model.NYCSchool
import retrofit2.http.GET

interface ApiService {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolsData(): List<NYCSchool>
}