package com.company.nycschoolphotontest.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.company.nycschoolphotontest.model.NYCSchool
import com.company.nycschoolphotontest.repository.NycSchoolsRepository
import kotlinx.coroutines.launch

class NycSchoolsViewModel: ViewModel() {
    val repository = NycSchoolsRepository()

    var schools by mutableStateOf<List<NYCSchool>>(listOf())
        private set

    var isLoading by mutableStateOf(false)

    init {
        fetchSchools()
    }

    private fun fetchSchools() {
        viewModelScope.launch {
            isLoading = true
            try {
               schools =  repository.getSchools()
            }
            catch (e:Exception){
                Log.e("NYCViewModel", "Error getting Schools ", e)
            }
            finally {
                isLoading = false
            }
        }
    }

}