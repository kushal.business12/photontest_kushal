package com.company.nycschoolphotontest

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.company.nycschoolphotontest.model.NYCSchool
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@MediumTest
class MainActivityTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun `schoolList_displaysSchools_Correctly`(){

        val schools = listOf(NYCSchool("DBN", "SchoolName1","OverviewParagrpgh1"),
            NYCSchool("DBN2", "SchoolName2","OverviewParagrpgh2"))
        composeTestRule.setContent {
            SchoolList(schools = schools, navController = rememberNavController() )
        }
        schools.forEach {nycSchool ->
            composeTestRule.onNodeWithText("DBN: "+nycSchool.dbn).assertIsDisplayed()
        }

    }


    @Test
    fun schoolItem_displaysSchoolListCorrectly(){
        val school = NYCSchool("123","Test School", "Overview")

        composeTestRule.setContent {
            SchoolItem(school) {}
        }
        composeTestRule.onNodeWithText("DBN: 123").assertIsDisplayed()
        composeTestRule.onNodeWithText("School Name: Test School").assertIsDisplayed()

    }

    @Test
    fun clickingSchoolItem_triggersOnClick(){
        val school = NYCSchool("123","Test School", "Overview")
        var clickedSchool: NYCSchool? = null
        composeTestRule.setContent {
            SchoolItem(school) { clickedSchool =it}
        }

        composeTestRule.onNodeWithText("School Name: Test School").performClick()
        assert(clickedSchool == school)
        }






}