package com.company.nycschoolphotontest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.company.nycschoolphotontest.model.NYCSchool
import com.company.nycschoolphotontest.ui.theme.NYCSchoolPhotonTestTheme
import com.company.nycschoolphotontest.viewmodel.NycSchoolsViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp()
        }
    }
}

@Composable
fun MyApp() {
    NYCSchoolPhotonTestTheme {
        AppNavigation()
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DisplaySchools(navController: NavController?) {
    val viewModel: NycSchoolsViewModel = viewModel()
    val schoolData = viewModel.schools
    val isLoading = viewModel.isLoading

    Column {
        TopAppBar(title = { Text("NYC Schools") })

        if (isLoading) {
            LoadingIndicator()
        } else {
            SchoolList(schoolData, navController)
        }
    }
}

@Composable
fun SchoolList(schools: List<NYCSchool>, navController: NavController?) {
    LazyColumn {
        items(schools) { school ->
            SchoolItem(school) { selectedItem ->
                navController?.navigate("screen_2/${selectedItem.overview_paragraph}")
            }
        }
    }
}


@Composable
fun SchoolItem(school: NYCSchool, onClick: (NYCSchool) -> Unit) {

    Column(modifier = Modifier
        .fillMaxWidth()
        .padding(16.dp)
        .clickable { onClick(school) }) {
        Text(
            text = "DBN: ${school.dbn}",
            style = MaterialTheme.typography.bodyMedium,
            fontSize = 16.sp
        )
        Text(
            text = "School Name: ${school.school_name}",
            style = MaterialTheme.typography.bodyMedium,
            fontSize = 16.sp
        )
    }
    Divider(
        modifier = Modifier
            .fillMaxWidth()
            .height(1.dp), color = Color.Gray
    )
}

@Composable
fun LoadingIndicator() {
    Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
        CircularProgressIndicator()
    }
}
