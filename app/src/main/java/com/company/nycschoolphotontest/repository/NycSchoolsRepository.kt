package com.company.nycschoolphotontest.repository

import com.company.nycschoolphotontest.model.NYCSchool
import com.company.nycschoolphotontest.network.RetrofitServiceManager

class NycSchoolsRepository {
    private val apiService = RetrofitServiceManager.retrofit

    suspend fun getSchools():List<NYCSchool> {
        return apiService.getSchoolsData()
    }
}