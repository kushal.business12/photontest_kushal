package com.company.nycschoolphotontest.model

data class NYCSchool(
    val dbn: String,
    val school_name: String,
    val overview_paragraph: String
)
