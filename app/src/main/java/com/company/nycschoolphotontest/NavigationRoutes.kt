package com.company.nycschoolphotontest

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument

object NavigationRoutes {
    const val SCREEN_1 = "screen_1"
    const val SCREEN_2 = "screen_2/{overview_paragraph}"

}

@Composable
fun AppNavigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = NavigationRoutes.SCREEN_1) {
        composable(NavigationRoutes.SCREEN_1) {
            DisplaySchools(navController)
        }
        composable(NavigationRoutes.SCREEN_2,
            arguments = listOf(navArgument("overview_paragraph") { type = NavType.StringType })
        ) { backStackEntry ->
            val overViewParagrapgh = backStackEntry.arguments?.getString("overview_paragraph")
            DetailScreen(overViewParagrapgh, navController)

        }
    }
}
